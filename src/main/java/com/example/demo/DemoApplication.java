package com.example.demo;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

  @GetMapping("/")
  public String home() {
    return "Spring is here!";
  }

  @GetMapping("/hello/{name}")
  public String hello(@PathVariable String name) {
    return "Hello, " + name + "!";
  }

  // Generate a PUT Method that checks whether the first parameter is present in
  // MySql

  @PutMapping("/write/{name}")
  public int write(@PathVariable String name) {
    // Check if name exists in database
    return insertName(name);

  }

  // Define database properties
  private final String jdbcUrl = "jdbc:mysql://sql11.freemysqlhosting.net:3306/sql11677286";
  private final String username = "sql11677286";
  private final String password = "l3rkiCEhnB";

  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
    dataSource.setUrl(jdbcUrl);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    return dataSource;
  }

  @Bean
  public JdbcTemplate jdbcTemplate(DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

  @Autowired
  private JdbcTemplate jdbcTemplate;

  // Create a method that inserts a name into the database
  public int insertName(String name) {
    String sql = "INSERT INTO names (name) VALUES (?)";
    return jdbcTemplate.update(sql, name);
  }

  // Create a GET method that lists all values from the 'names' table
  @GetMapping("/names")
  public List<String> getNames() {
    String sql = "SELECT name FROM names";
    return jdbcTemplate.query(sql, (rs, rowNum) -> rs.getString("name"));
  }






  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }
}
